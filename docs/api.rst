API
----

You can build you own tests, validators and sanitizers using these classes.

.. module:: pyscrawl

.. autoclass:: Scrawl
    :members: upload_zipfile, upload_container, upload_stream

.. autoclass:: ScrawlContainer
    :members:

.. autoclass:: ScrawlResultDocument
    :members:
