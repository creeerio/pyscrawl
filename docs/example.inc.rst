Quickstart
----------

Quickstart with a pre-build zipfile:

.. code-block:: python

  # -*- coding: utf-8 -*-
  from pyscrawl import Scrawl

  # setup the Scrawl client with your API key
  scrawl = Scrawl('MY-API-KEY')

  # upload ./document.zip to Scrawl and wait for the conversion.
  result = scrawl.upload_zipfile('PyScrawl test', './document.zip')
  result.sleep_until_ready()

  # direct link to the generated PDF file
  pdf_file = result.pdf
