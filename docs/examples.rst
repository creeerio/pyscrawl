Example
-------

Quickstart with a pre-build zipfile:

.. code-block:: python

  # -*- coding: utf-8 -*-
  from pyscrawl import Scrawl

  # setup the Scrawl client with your API key
  scrawl = Scrawl('MY-API-KEY')

  # upload ./document.zip to Scrawl and wait for the conversion.
  result = scrawl.upload_zipfile('PyScrawl test', './document.zip')
  result.sleep_until_ready()

  # direct link to the generated PDF file
  pdf_file = result.pdf

You can also build a zipfile on the fly using `ScrawlDocument`.

.. code-block:: python

  # -*- coding: utf-8 -*-
  from pyscrawl import Scrawl, ScrawlDocument

  # setup the Scrawl client with your API key
  scrawl = Scrawl('MY-API-KEY')

  # define an index_html using a template engine (for example)
  index_html = '''
  <html>
    <head>
      <link rel="stylesheet" href="./styles.css">
    </head>
    <body>
      <img src="./awesome-logo.png" alt="Awesome logo" width="150px" height="150px"/>
      <h1>Hello World</h1>
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla
      accumsan, metus ultrices eleifend gravida, nulla nunc varius lectus,
      nec rutrum justo nibh eu lectus. Ut vulputate semper dui. Fusce erat
      odio, sollicitudin vel erat vel, interdum mattis neque.</p>
    </body>
  </html>
  '''

  # build a new document
  document = (ScrawlDocument()
              .add_file('./styles.css')
              .add_file('./awesome-logo.png')
              .add_content('index.html', index_html))

  # upload to Scrawl and wait for the conversion.
  result = scrawl.upload_document('PyScrawl document', document)
  result.sleep_until_ready()

  # direct link to the generated PDF file
  pdf_file = result.pdf
