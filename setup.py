# -*- coding: utf-8 -*-
from setuptools import setup

setup(name='PyScrawl',
      version=open('VERSION').read().strip(),
      packages=['pyscrawl'],
      url='https://bitbucket.org/creeerio/pyscrawl',

      author='Nils',
      author_email='nils@creeer.io',
      license='MIT',

      description='API client for using scrawl.nl.',
      long_description=open('README.rst').read(),

      classifiers=[
          # 3 - Alpha, 4 - Beta, 5 - Production/Stable
          'Development Status :: 3 - Alpha',
          'Intended Audience :: Developers',
          'License :: OSI Approved :: MIT License',
          'Programming Language :: Python :: 3.5',
          'Programming Language :: Python :: 2.7',
      ],

      install_requires=['requests'],
      setup_requires=['pytest-runner'],
      tests_require=['pytest', 'pytest-cov', 'pytest-pep8'])
