# -*- coding: utf-8 -*-
from os import path

import pyscrawl

API_KEY = open(path.dirname(__file__) + '/../APIKEY.txt').read().strip()


def test_upload_zipfile():
    # setup a new Scrawl using your API KEY
    scrawl = pyscrawl.Scrawl(API_KEY)

    # upload the zipfile to Scrawl
    filename = path.dirname(__file__) + '/simple-test/simple-test.zip'
    result = scrawl.upload_zipfile('PyScrawl zipfile test', filename)

    # after uploading, PDF conversion has not yet been done
    assert result.converted is False

    # wait for the Scrawl service to convert our document
    result.sleep_until_ready()

    # the PDF is now ready to be downloaded / viewed
    assert result.converted is True
    assert result.pdf.endswith('pyscrawl-zipfile-test.pdf')


def test_upload_container():
    # setup a new Scrawl using your API KEY
    scrawl = pyscrawl.Scrawl(API_KEY)

    # prepare some "build" content
    base = path.dirname(__file__) + '/simple-test'

    with open(base + '/index.html') as stream:
        index_html = stream.read()

    # start a new ScrawlContainer and add the content and some static files
    document = (pyscrawl.ScrawlContainer()
                .add_content('index.html', index_html)
                .add_file(base + '/simple-test.css')
                .add_file(base + '/simple-test.jpg'))

    # upload the document to Scrawl
    result = scrawl.upload_container('PyScrawl document test', document)

    # after uploading, PDF conversion has not yet been done
    assert result.converted is False

    # wait for the Scrawl service to convert our document
    result.sleep_until_ready()

    # the PDF is now ready to be downloaded / viewed
    assert result.converted is True
    assert result.pdf.endswith('pyscrawl-document-test.pdf')
